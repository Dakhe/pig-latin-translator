<?php


namespace App\Services;


interface IPigLatinTranslatorService
{
    public function translate(string $inputText): string;
}