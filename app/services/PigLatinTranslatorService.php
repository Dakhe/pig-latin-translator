<?php

namespace App\Services;

use App\Services\PigLatinTranslatorService\IPigLatinTranslatorFactory;

class PigLatinTranslatorService implements IPigLatinTranslatorService
{
    private $pigLatinTranslatorFactory;

    public function __construct(IPigLatinTranslatorFactory $pigLatinTranslatorFactory)
    {
        $this->pigLatinTranslatorFactory = $pigLatinTranslatorFactory;
    }

    public function translate(string $inputText): string
    {
        $translatedText = '';
        $currentWord = '';
        $inputLength = \strlen($inputText);

        for($i = 0; $i < $inputLength; $i++)
        {
            $char = $inputText[$i];
            if(ctype_alpha($char))
            {
                $currentWord .= $char;
            }
            else
            {
                $translatedText .= $this->translateWord($currentWord);
                $translatedText .= $char;
                $currentWord = '';
            }
        }

        if($currentWord !== '')
        {
            $translatedText .= $this->translateWord($currentWord);
        }

        return $translatedText;
    }

    private function translateWord($currentWord): string
    {
        $translatedWord = '';

        if($currentWord !== '')
        {
            $translator = $this->pigLatinTranslatorFactory->create($currentWord);
            $translatedWord = $translator->translate($currentWord);
        }

        return $translatedWord;
    }
}