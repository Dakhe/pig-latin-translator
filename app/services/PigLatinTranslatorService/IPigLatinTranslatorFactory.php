<?php


namespace App\Services\PigLatinTranslatorService;


interface IPigLatinTranslatorFactory
{
    public function create(string $inputWord): IPigLatinTranslator;
}