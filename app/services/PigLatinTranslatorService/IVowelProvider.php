<?php


namespace App\Services\PigLatinTranslatorService;


interface IVowelProvider
{
    public function getVowels(int $i): array;
}