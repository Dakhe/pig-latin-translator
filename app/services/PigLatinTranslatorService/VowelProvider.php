<?php


namespace App\Services\PigLatinTranslatorService;


class VowelProvider implements IVowelProvider
{
    public function getVowels(int $i): array
    {
        if($i === 0)
        {
            $vowels = $this->getVowelsAtWordBeginning();
        }
        else
        {
            $vowels = $this->getVowelsInsideWord();
        }

        return $vowels;
    }

    private function getVowelsAtWordBeginning(): array
    {
        return array('a', 'e', 'i', 'o', 'u');
    }

    private function getVowelsInsideWord(): array
    {
        return array('a', 'e', 'i', 'o', 'u', 'y');
    }
}