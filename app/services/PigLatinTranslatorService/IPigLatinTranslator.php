<?php


namespace App\Services\PigLatinTranslatorService;


interface IPigLatinTranslator
{
    public function translate(string $inputWord): string;
}