<?php


namespace App\Services\PigLatinTranslatorService;


class VowelPigLatinTranslator implements IPigLatinTranslator
{

    public function translate(string $inputWord): string
    {
        return $inputWord . '-ay';
    }
}