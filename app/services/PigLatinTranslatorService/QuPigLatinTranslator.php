<?php

namespace App\Services\PigTranslatorService;

use App\Services\PigLatinTranslatorService\IPigLatinTranslator;

class QuPigLatinTranslator implements IPigLatinTranslator
{
    public function translate(string $inputText): string
    {
        $initialQu = \substr($inputText, 0, 2);
        return \substr($inputText, 2) . '-' . $initialQu . 'ay';
    }
}
