<?php

namespace App\Services\PigLatinTranslatorService;

class ConsonantPigLatinTranslator implements IPigLatinTranslator
{
    private $vowelProvider;
    private $dialectAffixProvider;

    public function __construct(IVowelProvider $vowelProvider, IDialectAffixProvider $dialectAffixProvider)
    {
        $this->vowelProvider = $vowelProvider;
        $this->dialectAffixProvider = $dialectAffixProvider;
    }

    public function translate(string $inputWord): string
    {
        $consonantsCluster = '';
        $i = 0;

        while($i < \strlen($inputWord))
        {
            $vowels = $this->vowelProvider->getVowels($i);

            if(!\in_array($inputWord[$i], $vowels, true))
            {
                $consonantsCluster .= $inputWord[$i];
                $i++;
            }
            else
            {
                break;
            }
        }

        return substr($inputWord, $i) . '-' . $consonantsCluster . $this->dialectAffixProvider->getAffix() . 'ay';
    }
}
