<?php


namespace App\Services\PigLatinTranslatorService;

use App\Services\PigTranslatorService\QuPigLatinTranslator;

class PigLatinTranslatorFactory implements IPigLatinTranslatorFactory
{
    private $vowelProvider;
    private $dialectAffixProvider;

    public function __construct(IVowelProvider $vowelProvider, IDialectAffixProvider $dialectAffixProvider)
    {
        $this->vowelProvider = $vowelProvider;
        $this->dialectAffixProvider = $dialectAffixProvider;
    }

    /**
     * @param string $inputWord
     * @return IPigLatinTranslator
     * @throws \InvalidArgumentException
     */
    public function create(string $inputWord): IPigLatinTranslator
    {
        if('' === $inputWord)
        {
            throw new \InvalidArgumentException("Provided string '{$inputWord}' is not a word.");
        }

        $word = strtolower($inputWord);

        if (\strlen($word) > 1 && $word[0] === 'q' && $word[1] === 'u')
        {
            $translator = new QuPigLatinTranslator();
        }
        elseif(\in_array($word[0], $this->vowelProvider->getVowels(0), true))
        {
            $translator = new VowelPigLatinTranslator();
        }
        else
        {
            $translator = new ConsonantPigLatinTranslator($this->vowelProvider, $this->dialectAffixProvider);
        }

        return $translator;
    }
}