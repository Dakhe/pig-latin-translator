<?php


namespace App\Services\PigLatinTranslatorService;


interface IDialectAffixProvider
{
    public function getAffix(): string;
}