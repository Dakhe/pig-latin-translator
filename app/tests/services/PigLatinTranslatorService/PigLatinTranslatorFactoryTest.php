<?php

namespace Tests\Services\PigLatinTranslatorService;

use App\Services\PigLatinTranslatorService\ConsonantPigLatinTranslator;
use App\Services\PigLatinTranslatorService\DialectAffixProvider;
use App\Services\PigLatinTranslatorService\PigLatinTranslatorFactory;
use App\Services\PigLatinTranslatorService\VowelPigLatinTranslator;
use App\Services\PigLatinTranslatorService\VowelProvider;
use App\Services\PigTranslatorService\QuPigLatinTranslator;
use PHPUnit\Framework\TestCase;

class PigLatinTranslatorFactoryTest extends TestCase
{
    public function testTranslatorFactory_EmptyInput_ExceptionIsThrown(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $translatorFactory = new PigLatinTranslatorFactory(new VowelProvider(), new DialectAffixProvider());
        $translator = $translatorFactory->create('');
    }

    public function testTranslatorFactory_InputConsonant_ConsonantTranslatorIsCreated(): void
    {
        $translatorFactory = new PigLatinTranslatorFactory(new VowelProvider(), new DialectAffixProvider());
        $translator = $translatorFactory->create('b');

        $this->assertInstanceOf(
            ConsonantPigLatinTranslator::class,
            $translator,
            "ConsonantTranslator should be created after 'b'"
        );
    }

    public function testTranslatorFactory_InputVowel_VowelTranslatorIsCreated(): void
    {
        $translatorFactory = new PigLatinTranslatorFactory(new VowelProvider(), new DialectAffixProvider());
        $translator = $translatorFactory->create('e');

        $this->assertInstanceOf(
            VowelPigLatinTranslator::class,
            $translator,
            "VowelTranslator should be created after 'e'"
        );
    }

    public function testTranslatorFactory_InputQ_ConsonantTranslatorIsCreated(): void
    {
        $translatorFactory = new PigLatinTranslatorFactory(new VowelProvider(), new DialectAffixProvider());
        $translator = $translatorFactory->create('q');

        $this->assertInstanceOf(
            ConsonantPigLatinTranslator::class,
            $translator,
            "ConsonantTranslator should be created after 'q'"
        );
    }

    public function testTranslatorFactory_InputQu_QuTranslatorIsCreated(): void
    {
        $translatorFactory = new PigLatinTranslatorFactory(new VowelProvider(), new DialectAffixProvider());
        $translator = $translatorFactory->create('qu');

        $this->assertInstanceOf(
            QuPigLatinTranslator::class,
            $translator,
            "QuTranslator should be created after 'qu'"
        );
    }
}
