<?php

namespace Tests\Services\PigLatinTranslatorService;

use App\Services\PigLatinTranslatorService\VowelPigLatinTranslator;
use PHPUnit\Framework\TestCase;

class VowelPigLatinTranslatorTest extends TestCase
{
    public function testVowelPigLatinTranslator_CorrectInput_TranslationOk(): void
    {
        $translator = new VowelPigLatinTranslator();

        foreach($this->getTestData() as $input => $expectedResult)
        {
            $result = $translator->translate($input);
            $this->assertEquals($expectedResult, $result);
        }
    }

    private function getTestData(): array
    {
        $words = array(
            'eagle'     => 'eagle-ay',
        );

        return $words;
    }

}
