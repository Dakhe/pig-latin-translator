<?php

namespace Tests\Services\PigLatinTranslatorService;

use App\Services\PigLatinTranslatorService\ConsonantPigLatinTranslator;
use App\Services\PigLatinTranslatorService\DialectAffixProvider;
use App\Services\PigLatinTranslatorService\IDialectAffixProvider;
use App\Services\PigLatinTranslatorService\VowelProvider;
use PHPUnit\Framework\TestCase;

class ConsonantPigLatinTranslatorTest extends TestCase
{
    public function testConsonantPigLatinTranslator_CorrectInput_TranslationOk(): void
    {
        $translator = new ConsonantPigLatinTranslator(new VowelProvider(), new DialectAffixProvider());

        foreach($this->getTestData() as $input => $expectedResult)
        {
            $result = $translator->translate($input);
            $this->assertEquals($expectedResult, $result);
        }
    }

    public function testConsonantPigLatinTranslator_YAsConsonant_TranslationOk(): void
    {
        $translator = new ConsonantPigLatinTranslator(new VowelProvider(), new DialectAffixProvider());
        $result = $translator->translate('yellow');
        $this->assertEquals('ellow-yay', $result);
    }

    public function testConsonantPigLatinTranslator_YAsVowel_TranslationOk(): void
    {
        $translator = new ConsonantPigLatinTranslator(new VowelProvider(), new DialectAffixProvider());
        $result = $translator->translate('style');
        $this->assertEquals('yle-stay', $result);
    }

    public function testConsonantPigLatinTranslator_NonEmptyDialectAffixProvided_DialectAffixIsIncludedInTranslation(): void
    {
        $wowDialectAffixProviderMock = $this->createMock(IDialectAffixProvider::class);
        $wowDialectAffixProviderMock->method('getAffix')
            ->willReturn('wow');

        $translator = new ConsonantPigLatinTranslator(new VowelProvider(), $wowDialectAffixProviderMock);
        $result = $translator->translate('trust');
        $this->assertEquals('ust-trwoway', $result);
    }

    private function getTestData(): array
    {
        $words = array(
            'beast'     => 'east-bay',
            'dough'     => 'ough-day',
            'happy'     => 'appy-hay',
            'star'      => 'ar-stay',
            'three'     => 'ee-thray',
            'you'       => 'ou-yay',
        );

        return $words;
    }
}
