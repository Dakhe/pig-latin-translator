<?php

namespace Tests\Services\PigLatinTranslatorService;

use App\Services\PigTranslatorService\QuPigLatinTranslator;
use PHPUnit\Framework\TestCase;

class QuPigLatinTranslatorTest extends TestCase
{
    public function testQuPigLatinTranslator_CorrectInput_TranslationOk(): void
    {
        $translator = new QuPigLatinTranslator();

        foreach($this->getTestData() as $input => $expectedResult)
        {
            $result = $translator->translate($input);
            $this->assertEquals($expectedResult, $result);
        }
    }

    private function getTestData(): array
    {
        $words = array(
            'question'  => 'estion-quay',
            'quiet'     => 'iet-quay',
        );

        return $words;
    }
}
