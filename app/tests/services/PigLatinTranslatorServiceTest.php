<?php

namespace Tests\Services;

use App\Services\PigLatinTranslatorService;
use App\Services\PigLatinTranslatorService\DialectAffixProvider;
use App\Services\PigLatinTranslatorService\PigLatinTranslatorFactory;
use App\Services\PigLatinTranslatorService\VowelProvider;
use PHPUnit\Framework\TestCase;

class PigLatinTranslatorServiceTest extends TestCase
{
    public function testPigTranslatorService_MultipleWordsInput_IsTranslationOk(): void
    {
        $service = new PigLatinTranslatorService(new PigLatinTranslatorFactory(new VowelProvider(), new DialectAffixProvider()));
        $input = 'beast dough happy question star three';
        $expectedOutput = 'east-bay ough-day appy-hay estion-quay ar-stay ee-thray';

        $output = $service->translate($input);

        $this->assertSame($expectedOutput, $output, 'the translation should be correct');
    }

    public function testPigTranslatorService_ComplexSentencesInput_IsTranslationOk(): void
    {
        $service = new PigLatinTranslatorService(new PigLatinTranslatorFactory(new VowelProvider(), new DialectAffixProvider()));
        $input = 'Pig Latin is a language game or argot in which words in English are altered, usually by adding '
            . 'a fabricated suffix or by moving the onset or initial consonant or consonant cluster of a word to the end '
            . 'of the word and adding a vocalic syllable to create such a suffix. The objective is to conceal the words '
            . 'from others not familiar with the rules. The reference to Latin is a deliberate misnomer; '
            . 'Pig Latin is simply a form of argot or jargon unrelated to Latin, and the name is used for its English '
            . 'connotations as a strange and foreign-sounding language.';

        $expectedOutput = 'ig-Pay atin-Lay is-ay a-ay anguage-lay ame-gay or-ay '
            . 'argot-ay in-ay ich-whay ords-way in-ay English-ay are-ay '
            . 'altered-ay, usually-ay y-bay adding-ay a-ay abricated-fay '
            . 'uffix-say or-ay y-bay oving-may e-thay onset-ay or-ay '
            . 'initial-ay onsonant-cay or-ay onsonant-cay uster-clay of-ay '
            . 'a-ay ord-way o-tay e-thay end-ay of-ay e-thay ord-way and-ay '
            . 'adding-ay a-ay ocalic-vay yllable-say o-tay eate-cray uch-say '
            . 'a-ay uffix-say. e-Thay objective-ay is-ay o-tay onceal-cay '
            . 'e-thay ords-way om-fray others-ay ot-nay amiliar-fay ith-way '
            . 'e-thay ules-ray. e-Thay eference-ray o-tay atin-Lay is-ay a-ay '
            . 'eliberate-day isnomer-may; ig-Pay atin-Lay is-ay imply-say '
            . 'a-ay orm-fay of-ay argot-ay or-ay argon-jay unrelated-ay '
            . 'o-tay atin-Lay, and-ay e-thay ame-nay is-ay used-ay or-fay '
            . 'its-ay English-ay onnotations-cay as-ay a-ay ange-stray '
            . 'and-ay oreign-fay-ounding-say anguage-lay.';

        $output = $service->translate($input);

        $this->assertSame($expectedOutput, $output, 'the translation should be correct');
    }
}
