<?php

namespace App\Presenters;

use App\Services\IPigLatinTranslatorService;
use Nette;
use Nette\Application\UI\Form;


class HomepagePresenter extends Nette\Application\UI\Presenter
{
    private $pigTranslatorService;

    public function __construct(IPigLatinTranslatorService $pigTranslatorService)
    {
        parent::__construct();
        $this->pigTranslatorService = $pigTranslatorService;
    }

    protected function createComponentInputForm(): Form
    {
        $form = new Form;

        $form->addProtection();

        $form->addTextArea('input', 'Text to translate:')
            ->setRequired();

        $form->addSubmit('send', 'Translate');

        $form->onSuccess[] = [$this, 'inputFormSucceeded'];

        return $form;
    }

    public function inputFormSucceeded(Form $form, $values): void
    {
        $translatedText = $this->pigTranslatorService->translate($values->input);

        $this->flashMessage("An old Roman pig would say: '{$translatedText}'", 'success');
        $this->redirect('this');
    }
}
