# Pig Latin Translator

My first project in Nette is going to be a Pig Latin Translator :) Let's go!

## Known functional limitations:
* the translation is not perfect :-)
    * 'y' at the word beginning is considered to be always a consonant while 'y' inside a word is considered to be a vowel
    * hyphen is used to simplify the translation back to English
* capitalization is not handled (input capital letters are preserved in the translation)
* 'local dialects' is supported and can be customized via custom implementation of IDialectAffixProvider
* current translation does not work with multi-byte strings 

## Code limitations:
* no performance tuning/benchmarking was done (there might be faster translators out there)
* security is handled by inbuilt Latte escaping capabilities

